// 
const http = require('http');

// Стартуем сервер
require('./server');

let firstnames = ['Joey', 'Chandler', 'Ross'];
let lastnames = ['Tribbiani', 'Bing', 'Geller'];

firstnames.forEach(function(firstname, i, array) {
	lastnames.forEach(function(lastname, i, array) {
		setTimeout(function() {
			sendRequest(firstname, lastname)			
		}, 1000);
	});	
})

function sendRequest(firstName, lastName) {
	
	var request = http.request({
		host: "127.0.0.1",
		port: "3000",
		method: "GET",
		headers: {
			'firstName': firstName,
			'lastName': lastName
		}
	});
	request.on('response', response => {
		let data = '';
		response.on('data', chunk => data += chunk );
		response.on('end', () => console.log(data));
		response.on('error', error => console.error(error));
	});
	request.on('error', error => console.log(error) );
	request.end();
	
}