const externalservice = require('./external-service-client');

const http = require('http');
const port = 3000;

function handler(req, res) {
	
	let headers = req.headers;
	console.log(`${req.method} headers: ${JSON.stringify(headers)}`);
	
	externalservice.getSecretKey(
		headers.firstname,
		headers.lastname,
		(error, secretKey) => {
		
			if(error) {
				res.writeHead(500,'ERROR',{'Content-Type': 'application/json'});
				res.write(JSON.stringify(error));
				console.error(error);
			} else {
				res.writeHead(200,'OK',{'Content-Type': 'application/json'});
				res.write(JSON.stringify({
					firstName: headers.firstname,
					lastName: headers.lastname,
					secretKey: secretKey
				}));
			}
			res.end();					
		}
	);
}

const server = http.createServer();
server.on('error', err => console.error(err));
server.on('request', handler);
server.on('listening', () => {
	console.log('Start HTTP on port %d', port);
});
server.listen(port);
