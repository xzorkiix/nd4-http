const http = require('http');

module.exports.getSecretKey = (firstname, lastname, callback) => {
	
	if(!firstname)
		return callback('Не задан параметр firstname!');
	
	if(!lastname)
		return callback('Не задан параметр lastname!');

	var request = http.request({
		hostname: 'netology.tomilomark.ru',
		path: '/api/v1/hash',
		method: 'POST',
		headers: {
			'firstname': firstname,
			'Content-Type': 'application/json' 
		}
	});
	request.write(
		JSON.stringify({lastName: lastname})
	);
	
	request.on('error', 
		error => callback(error)
	);
	request.on('response',
		response => {
		
			// проверка статуса ответа сервера
			if (response.statusCode !== 200) {
				let error = Error(`Получен ответ ${response.statusCode} ${response.statusMessage}`);
				return callback(error);
			}
		
			let data = '';
			response.on('data', chunk => data += chunk );
			response.on('end', () => {
				var error, hash;
				try{
					hash = JSON.parse(data).hash;
				} catch(e) {
					error = new Error(e);
				}
				callback(error, hash);
			});
			response.on('error',
				error => callback(error));
			}
	);
	request.end();
};